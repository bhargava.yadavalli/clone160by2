import os
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))


class Config(object):
    SECRET_KEY = os.environ.get("SECRET_KEY")# or "b948d1d663b179e07c4e25ba32c34d6e"

    SQLALCHEMY_DATABASE_URI = os.environ.get('SQL_DB')# or os.environ.get('DATABASE_URL') or "sqlite:///" + os.path.join(basedir,"160by2db.db")
    
    SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get('SQLALCHEMY_TRACK_MODIFICATIONS') #or False
    
    MESSAGE_API_KEY = os.environ.get('MESSAGE_API_KEY')# or 'AFZOPAFEHD13SVMTRKHXKLVP5OVY9M4D'
    
    MESSAGE_SECRET_KEY = os.environ.get('MESSAGE_SECRET_KEY')# or "GA9QMGARQWHVTXQW"
    
    MESSAGE_USE_TYPE = os.environ.get('MESSAGE_USE_TYPE') #or "stage"
    
    SENDER_ID = "160by2"
    
    MESSAGE_URL = os.environ.get('MESSAGE_URL') #or 'https://www.160by2.com/api/v1/sendCampaign'
    ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL')
    MESSAGES_PER_PAGE = 5
    
    LOG_TO_STDOUT = os.environ.get('LOG_TO_STDOUT')

    MAIL_SERVER = os.environ.get("MAIL_SEVER") #or 'smtp.googlemail.com'
    MAIL_PORT = os.environ.get("MAIL_PORT") #or 587
    MAIL_USE_TLS = os.environ.get("MAIL_USE_TLS")# or True
    MAIL_USERNAME = os.environ.get("MAIL_USERNAME")# or "flask.mailsender1"
    MAIL_PASSWORD = os.environ.get("MAIL_PASSWORD") #or "1@mailsender"

    ADMINS = os.environ.get("ADMINS") #or "bhargava.yadavalli@mountblue.io"
    
    CELERY_BROKER_URL = 'redis://localhost:6379/0'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'