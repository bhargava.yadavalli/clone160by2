import csv
from project import celery
from project.models import Message
from flask import url_for
import os
@celery.task(bind=True, name="Download task")
def create_file(self, data):
    location = data['location']
    user_id = data['user_id']
    print("async createfile")
    messages = Message.query.filter_by(sender_id=user_id).all()
    length = len(messages)
    print('database completed', type(messages))
    basedir = os.path.abspath(os.path.dirname('project/static/'))
    print(basedir)
    location = os.path.join(basedir, f'messages{user_id}.csv')
    print(location)
    print(length)
    current_progress = 0
    with open(location, 'w') as dest_file:
        file_writer = csv.writer(dest_file, quoting=csv.QUOTE_ALL, delimiter=',')
        header = ['Id', 'Recipient_name', 'Message body']
        file_writer.writerow(header)
        for message in messages:
            row = [str(message.id), str(message.recipient.contact_name), str(message.message_body)]
            file_writer.writerow(row)
            current_progress += 1
            self.update_state(state='PROGRESS', meta={'current': current_progress, 'total': length, 'status': 'pending'})
    return {'current': 100, 'total': 100, 'status': 'Task completed!', 'result': location}