const create_download_file = (createFileUrl) => {
    fetch(createFileUrl)
    .then(res => res.json())
    .then((response) => {
        download_status(response['location'])

    });
}
const download_status = (downloadStatusUrl) => {
    fetch(downloadStatusUrl)
    .then(res => res.json())
    .then(res => {
            if(res['current'] == 100) {
                document.getElementById('alert-messages').class="alert alert-success"
                document.getElementById('message').innerHTML = "Your download has started please check downloads"
                let a = document.createElement('a')
                a.href = res['result']
                a.download = res['result'].split('/').pop()
                document.body.appendChild(a)
                a.click()
                document.body.removeChild(a)
            }
            else {
                document.getElementById('alert-messages').class="alert alert-info"
                document.getElementById('message').innerHTML = "Your download is getting ready please wait"
                setTimeout(function() {
                    download_status(downloadStatusUrl);
                }, 1000);
            }
        
    });
    
}