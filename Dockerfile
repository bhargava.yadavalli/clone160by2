FROM python:3.6-alpine

RUN adduser -D clone160by2
RUN apk update && apk add python3-dev
RUN apk add libxml2-dev libxslt-dev libffi-dev gcc musl-dev libgcc openssl-dev curl
RUN apk add --no-cache build-base zlib-dev
RUN apk add libjpeg zlib tiff freetype lcms libwebp tcl openjpeg

WORKDIR /home/clone160by2

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install --upgrade pip
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn pymysql

COPY project project
COPY migrations migrations
COPY clone160by2.py config.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP clone160by2.py

RUN chown -R clone160by2:clone160by2 ./
USER clone160by2

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]


